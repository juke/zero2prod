ci:	ci-rust-deps ci-cargo-deps ci-checks

ci-rust-deps:
	echo "Adding rust components...";
	rustup component add rustfmt;
	rustup component add clippy;

ci-cargo-deps:
	echo "Installing cargo components...";
	cargo install cargo-tarpaulin;
	cargo install cargo-audit;

ci-checks: ci-fmt ci-clippy ci-build ci-test ci-audit ci-tarpaulin

ci-fmt:
	echo " FMT";
	cargo fmt --all -- --check

ci-clippy:
	echo " CLIPPY";
	cargo clippy -- -D warnings

ci-build:
	echo " BUILD";
	cargo build

ci-test:
	echo " TEST";
	cargo test

ci-audit:
	echo " AUDIT";
	cargo audit

ci-tarpaulin:
	echo " TARPAULIN";
	cargo tarpaulin --ignore-tests

